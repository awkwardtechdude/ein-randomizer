# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'teinconfig.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dialog(object):
    def setupUi(self, dialog):
        dialog.setObjectName("dialog")
        dialog.resize(221, 190)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(dialog.sizePolicy().hasHeightForWidth())
        dialog.setSizePolicy(sizePolicy)
        self.verticalLayoutWidget = QtWidgets.QWidget(dialog)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 200, 174))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.dialog_vertical = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.dialog_vertical.setContentsMargins(0, 0, 0, 0)
        self.dialog_vertical.setObjectName("dialog_vertical")
        self.dialog_grid = QtWidgets.QGridLayout()
        self.dialog_grid.setVerticalSpacing(4)
        self.dialog_grid.setObjectName("dialog_grid")
        self.number_levels_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.number_levels_label.setObjectName("number_levels_label")
        self.dialog_grid.addWidget(self.number_levels_label, 0, 0, 1, 1)
        self.number_levels_entry = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.number_levels_entry.setObjectName("number_levels_entry")
        self.dialog_grid.addWidget(self.number_levels_entry, 0, 1, 1, 1)
        self.dialog_vertical.addLayout(self.dialog_grid)
        self.randomizeMusicCheck = QtWidgets.QCheckBox(self.verticalLayoutWidget)
        self.randomizeMusicCheck.setObjectName("randomizeMusicCheck")
        self.dialog_vertical.addWidget(self.randomizeMusicCheck)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.randomizeColorPalleteCheck = QtWidgets.QCheckBox(self.verticalLayoutWidget)
        self.randomizeColorPalleteCheck.setObjectName("randomizeColorPalleteCheck")
        self.verticalLayout.addWidget(self.randomizeColorPalleteCheck)
        self.saveRandomizeButton = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.saveRandomizeButton.setObjectName("saveRandomizeButton")
        self.verticalLayout.addWidget(self.saveRandomizeButton)
        self.saveCloseButton = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.saveCloseButton.setObjectName("saveCloseButton")
        self.verticalLayout.addWidget(self.saveCloseButton)
        self.dialog_vertical.addLayout(self.verticalLayout)

        self.retranslateUi(dialog)
        QtCore.QMetaObject.connectSlotsByName(dialog)

    def retranslateUi(self, dialog):
        _translate = QtCore.QCoreApplication.translate
        dialog.setWindowTitle(_translate("dialog", "TEIN-Randomizer Config"))
        self.number_levels_label.setText(_translate("dialog", "# of levels"))
        self.randomizeMusicCheck.setText(_translate("dialog", "randomize music"))
        self.randomizeColorPalleteCheck.setText(_translate("dialog", "randomize color pallete"))
        self.saveRandomizeButton.setText(_translate("dialog", "Save and Randomize"))
        self.saveCloseButton.setText(_translate("dialog", "Save and Close"))

