# The End Is Nigh Randomizer

Randomizes and configures a given set of level names in the map.csv of The End Is Nigh


Put level names in levelpool.txt

See build and install instructions in [wiki](https://gitlab.com/awkwardtechdude/ein-randomizer/wikis/Running-and-Compiling).
