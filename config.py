from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import xml.etree.ElementTree as ET
from configui import Ui_dialog
import randomizer
sys.path.append('levelpool')
configTree = ET.parse('config.xml')
configRoot = configTree.getroot()

class ConfigApp(Ui_dialog):
    def __init__(self, dialog):
        Ui_dialog.__init__(self)
        self.setupUi(dialog)
        self.saveCloseButton.clicked.connect(self.exportValues)
        self.saveRandomizeButton.clicked.connect(self.saveRun)

    def exportValues(self):
        configRoot.find('numberlevels').text = self.number_levels_entry.text()

        configTree.write('config.xml')
        sys.exit()

    def saveRun(self):
        self.exportValues()
        randomizer.main()
        sys.exit()

    def fillFields(self):
        self.number_levels_entry.setText(configRoot.find('numberlevels').text)



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    dialog = QtWidgets.QDialog()
    prog = ConfigApp(dialog)
    prog.fillFields()
    dialog.show()
    sys.exit(app.exec_())