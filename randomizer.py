import secrets
import csv
import xml.etree.ElementTree as ET
import sys
import random
import os

#config
configtree = ET.parse('config.xml')
configroot = configtree.getroot()
settings = {}
for item in configroot:
    settings[item.tag] = item.text

number_entries = eval(settings['numberlevels'])
insertion_line_number = eval(settings['insertionline'])
insertion_column = eval(settings['insertioncolumn'])
input_file = settings['inputfile']
output_file = settings['outputfile']

default_music = configroot.find('default_music').text
area_type = configroot.find('area_type').text
area_name = configroot.find('area_name').text
musicrando = configroot.find('musicrando').text
paletterando = configroot.find('paletterando').text


def main():

    # load levelpool in read mode
#    levelpool_file = open((sys.path[0]+"/levelpool/"+'levelpool.txt'), 'r')
    levelpool = []
    activepools_file = open((sys.path[0]+"/levelpool/"+'activepools.txt'), 'r')
    ac_l = activepools_file.read().split("\n")
    for pool in ac_l:
        poolfile = open((sys.path[0]+"/levelpool/"+pool),'r')
        levelpool.extend(poolfile.read().split('\n'))
        poolfile.close()

    # load map spreadsheet in read mode
    mapsheet_file = open(input_file, 'r')

    # create csv reader of mapsheet
    map_csv = csv.reader(mapsheet_file, dialect="excel")

#    levelpool = levelpool_file.read().split("\n")

    randomized_pool = []

    # Choose levels
    for x in range(number_entries):
        random.shuffle(levelpool)
        random.SystemRandom().shuffle(levelpool)
        choice = random.SystemRandom().choice(levelpool)
        randomized_pool.append(choice)
        del levelpool[levelpool.index(choice)]

    # Add levels to levelinfo.txt

    levelinfo_t = open('levelinfo-t.txt', 'r')
    levelinfo_l = levelinfo_t.read().split('\n')

    for level in randomized_pool:
        levelname = level.split('.')[0]
        lvlinfo_entry = '"%s" {name="%s" id=-1}'%(levelname, "TEIN Randomizer "+(str(randomized_pool.index(level)+1)+"/"+str(number_entries)))
        print(lvlinfo_entry)
        levelinfo_l.append(lvlinfo_entry)
    print(levelinfo_l)
    levelinfo_t.close()
    levelinfo = open('levelinfo.txt', 'w')
    levelinfo.write("\n".join(levelinfo_l))



    print('Chosen Entries: ', randomized_pool)

    randomized_pool.append("v-end.lvl")

    map_list = [x for x in map_csv]
    ins_line = map_list[insertion_line_number]
    ins_line[insertion_column:insertion_column] = randomized_pool


    map_list[insertion_line_number] = ins_line

    # load map spreadsheet in write mode
    mapsheet_file.close()
    mapsheet_file = open(output_file, 'w')
    map_writer = csv.writer(mapsheet_file, dialect="unix")

    # write changes to file and close
    map_writer.writerows(map_list)
    mapsheet_file.close()

#opening tilesets-t.txt and vtilesets.txt
vtilesets = open('vtilesets.txt', 'r')
vtilesets_list = vtilesets.read().split('\n')

tilesets_t = open('tilesets-t.txt', 'r')
tilesets_list = tilesets_t.read().split('\n')
tilesets_t.close()

#set area name
for linenum in range(len(vtilesets_list)):
    if "area_name" in vtilesets_list[linenum]:
        vtilesets_list[linenum] = vtilesets_list[linenum] + (" "+area_name)

#set area type
for linenum in range(len(vtilesets_list)):
    if "area_type" in vtilesets_list[linenum]:
        vtilesets_list[linenum] = vtilesets_list[linenum] + (" "+area_type)

#music randomization
if musicrando == "1":
    musicpool_file = open('musicpool.txt', 'r')
    musicpool = musicpool_file.read().split("\n")
    random.SystemRandom().shuffle(musicpool)
    musicchoice = random.SystemRandom().choice(musicpool)
    for linenum in range(len(vtilesets_list)):
        if "music" in vtilesets_list[linenum]:
            vtilesets_list[linenum] = vtilesets_list[linenum] + (" "+musicchoice)
    musicpool_file.close()
else:
    for linenum in range(len(vtilesets_list)):
        if "music" in vtilesets_list[linenum]:
            vtilesets_list[linenum] = vtilesets_list[linenum] + (default_music)

#color palette randomization (
if paletterando == "1":
    palettepool_file = open('palettepool.txt', 'r')
    palettepool = palettepool_file.read().split("\n")
    for linenum in range(len(vtilesets_list)):
        if "palette" in vtilesets_list[linenum]:
            random.SystemRandom().shuffle(palettepool)
            palettechoice = random.SystemRandom().choice(palettepool)
            vtilesets_list[linenum] = vtilesets_list[linenum] + (" "+palettechoice)

#write to tilesets.txt  
tilesets_list.extend(vtilesets_list)
tilesets = open('tilesets.txt', 'w')
tilesets.write("\n".join(tilesets_list))
tilesets.close()


if __name__ == "__main__":
    main()
